import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GoogleService } from '../google.service';
import { CountryNews, User, WorldsNews } from '../models';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-news-country',
  templateUrl: './news-country.component.html',
  styleUrls: ['./news-country.component.css'],
  providers: [DatePipe]
})
export class NewsCountryComponent implements OnInit {

  newsDatabase: Array<CountryNews> | any;
  newsDatabase_dates: Array<Object> | any;
  newsDatabase_contents: Array<string> | any;
  newsDatabase_displayNames: Array<string> | any;
  date = new Date();
  content = new String;
  user: User | any;
  superusers: Array<Object> | any;
  userStatus = new String;
  country = '';


  newsForm = this.formBuilder.group({
    news: '',
  })

  constructor(
    private formBuilder: FormBuilder,
    public googleService: GoogleService,
    public datePipe: DatePipe, 
    private route: ActivatedRoute
  ) { }

  submit() {
    this.googleService.submitNewsCountry(this.content, this.country);
    this.newsForm.reset();
  }

  ngOnInit() {
    this.country = this.route.snapshot.params['country'];
    this.newsDatabase = [];
    this.googleService.getNewsCountry(this.country)
    .subscribe(response => {
      this.newsDatabase = response;
    })  
    this.user = this.googleService.getUser();
    this.googleService.getSuperUsers()
      .subscribe(response => {
      this.superusers = response;
      for (let object of this.superusers) {
        if (object.email == this.user.email) {
          this.userStatus = "super";
        }
        else {
          this.userStatus = "guest";
        }
      }
    })
  }

}

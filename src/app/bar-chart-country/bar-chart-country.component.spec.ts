import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartCountryComponent } from './bar-chart-country.component';

describe('BarChartCountryComponent', () => {
  let component: BarChartCountryComponent;
  let fixture: ComponentFixture<BarChartCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartCountryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

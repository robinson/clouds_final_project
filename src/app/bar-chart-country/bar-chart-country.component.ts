import { Component } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { SummaryData, DailyData} from '../models';


@Component({
  selector: 'app-bar-chart-country',
  templateUrl: './bar-chart-country.component.html',
  styleUrls: ['./bar-chart-country.component.css'],
  providers: [DatePipe]
})
export class BarChartCountryComponent {

  dailyData: DailyData | any;
  cases!: Array<number> | any;
  recovered!: Array<number> | any;
  deaths!: Array<number> | any;
  dates!: Array<string> | any;
  country = new String;
  countryName = new String;
  summaryData: SummaryData | any;
  selected_country: SummaryData | any;
  url = new String;

  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [ {}, {}, {} ];
  
  constructor(private service: DataService, private datePipe: DatePipe, private route: ActivatedRoute) {  }

  getCountryName() {
    this.service.getData().subscribe(
      response => {
        this.summaryData = response;
        for (let object of this.summaryData?.Countries) {
          if (object.Slug == this.country) {
            this.selected_country = object;
            this.countryName = this.selected_country.Slug;
          }
        }
        this.url = this.service.getURL_Daily(this.countryName);
        this.getAllData();
      }
    )
  }

  getAllData() {
    this.service.getData_7_days_country(this.url).subscribe(
      data => {
        this.dailyData = data;
        this.cases = Object.values(this.dailyData.timeline["cases"]);
        this.recovered = Object.values(this.dailyData.timeline["recovered"]);
        this.deaths = Object.values(this.dailyData.timeline["deaths"]);
        this.dates = Object.keys(this.dailyData.timeline["cases"]);
        for (let date of this.dates ) {
          let index = this.dates.indexOf(date);
          this.dates[index] = this.datePipe.transform(date, 'dd/MM/yy');
        }
        this.dates.splice(0, 1);
      }
    )
  }


  ngOnInit(): void {
    this.country = this.route.snapshot.params['country'];
    this.getCountryName();
    this.getAllData();
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PieChartCountryComponent } from './pie-chart-country.component';

describe('PieChartCountryComponent', () => {
  let component: PieChartCountryComponent;
  let fixture: ComponentFixture<PieChartCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PieChartCountryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PieChartCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { SummaryData } from '../models';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-pie-chart-country',
  templateUrl: './pie-chart-country.component.html',
  styleUrls: ['./pie-chart-country.component.css'], 
  providers: [DatePipe]
})
export class PieChartCountryComponent implements OnInit {

  summaryData: SummaryData | any;
  country = new String;
  selected_country: SummaryData | any;
  totalConfirmed: number | any;
  totalRecovered: number | any;
  totalDeaths: number | any;
  
  constructor(private service: DataService, private datePipe: DatePipe, private route: ActivatedRoute) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }
  
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['Dead Cases'], ['Recovered Cases'], ['Active Cases']];
  public pieChartData: SingleDataSet = [0, 0, 0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  ngOnInit(): void {
    this.country = this.route.snapshot.params['country'];
    this.getAllData();
  }

  getAllData() {
    this.service.getData().subscribe(
      response => {
        this.summaryData = response;
        for (let object of this.summaryData?.Countries) {
          if (object.Slug == this.country) {
            this.selected_country = object;
            this.totalConfirmed = this.selected_country.TotalConfirmed;
            this.totalRecovered = this.selected_country.TotalRecovered;
            this.totalDeaths = this.selected_country.TotalDeaths;
          }
        }
      }
    )
  }

}

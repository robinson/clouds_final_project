import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { SigninComponent } from './signin/signin.component';
import { RouterModule } from '@angular/router';

import { ChartsModule } from 'ng2-charts';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { SummaryTableComponent } from './summary-table/summary-table.component';
import { FooterComponent } from './footer/footer.component';
import { CountriesTableComponent } from './countries-table/countries-table.component';
import { MatTableModule } from '@angular/material/table';
import { HomeComponent } from './home/home.component';
import { CountryPageComponent } from './country-page/country-page.component';
import { SummaryTableCountryComponent } from './summary-table-country/summary-table-country.component';
import { BannerComponent } from './banner/banner.component';
import { BannerHomeComponent } from './banner-home/banner-home.component';
import { PieChartCountryComponent } from './pie-chart-country/pie-chart-country.component';
import { LineChartCountryComponent } from './line-chart-country/line-chart-country.component';
import { BarChartCountryComponent } from './bar-chart-country/bar-chart-country.component';
import { NewsComponent } from './news/news.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsCountryComponent } from './news-country/news-country.component';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    BarChartComponent,
    LineChartComponent,
    PieChartComponent,
    SummaryTableComponent,
    FooterComponent,
    CountriesTableComponent,
    HomeComponent,
    CountryPageComponent,
    SummaryTableCountryComponent,
    BannerComponent,
    BannerHomeComponent,
    PieChartCountryComponent,
    LineChartCountryComponent,
    BarChartCountryComponent,
    NewsComponent,
    NewsCountryComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
    MatTableModule,
    MatSelectModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    // AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { GoogleService } from '../google.service';
import { User } from '../models';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  user: User | any;
  superusers: Array<Object> | any;
  userStatus = new String;
  
  constructor(public googleService: GoogleService) { }

  ngOnInit(): void {
    this.user = this.googleService.getUser();
    this.googleService.getSuperUsers()
      .subscribe(response => {
      this.superusers = response;
      for (let object of this.superusers) {
        if (object.email == this.user.email) {
          this.userStatus = "super";
        }
        else {
          this.userStatus = "guest";
        }
      }
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { SummaryData } from '../models';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-summary-table',
  templateUrl: './summary-table.component.html',
  styleUrls: ['./summary-table.component.css'],
  providers: [DatePipe]
})
export class SummaryTableComponent implements OnInit {
  summaryData: SummaryData | any;
  
  constructor(private service: DataService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.getAllData();
  }

  getAllData() {
    this.service.getData().subscribe(
      response => {
        this.summaryData = response;
      }
    )
  }
}
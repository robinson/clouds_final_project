import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SummaryData } from '../models';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css', ], 
  providers: [DatePipe]
})
export class BannerComponent implements OnInit {

  summaryData: SummaryData | any;
  selected_country: SummaryData | any;
  country = new String;
  countryName = new String;

  constructor(private route: ActivatedRoute, private service: DataService, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.country = this.route.snapshot.params['country'];
    this.getAllData();
  }

  getAllData() {
    this.service.getData().subscribe(
      response => {
        this.summaryData = response;
        for (let object of this.summaryData?.Countries) {
          if (object.Slug == this.country) {
            this.selected_country = object;
            this.countryName = this.selected_country.Country;
          }
        }
      }
    )
  }

}

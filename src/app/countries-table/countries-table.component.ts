import {Component, OnInit} from '@angular/core';
import { SummaryData } from '../models';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';
import { orderBy } from "lodash";


@Component({
  selector: 'app-countries-table',
  templateUrl: './countries-table.component.html',
  styleUrls: ['./countries-table.component.css'],
  providers: [DatePipe]
})

export class CountriesTableComponent implements OnInit {
  summaryData: SummaryData | any;
  sortedData: SummaryData | any;
  
  constructor(private service: DataService, private datePipe: DatePipe) {  }

  ngOnInit() {
    this.getAllData();
  }
  
  getAllData() {
    this.service.getData().subscribe(
      response => {
        this.summaryData = response;
        this.sortData('Country', 'asc');
      }
    )
  }

  sortData(param, order) {
    this.sortedData = orderBy(this.summaryData?.Countries, param, order ) 
  }
}





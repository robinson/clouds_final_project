import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth'
import { User } from './models';
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class GoogleService {

  private user: User | any;
  news: any;
  constructor(
    private afAuth: AngularFireAuth, 
    private firestore: AngularFirestore) { }

  
  // User info related methods #########################################################################################################################
  async signInWithGoogle() {
    const credentials = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.user = {
      uid: credentials.user?.uid,
      displayName: credentials.user?.uid,
      email: credentials.user?.email
    };
    localStorage.setItem("user", JSON.stringify(this.user));
    this.updateUserData();
    window.location.reload();
  }

  updateUserData() {
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email
    }, {merge: true})
  }

  getUser() {
    if (this.user == null && this.userSignedIn()){
      this.user = JSON.parse(localStorage.getItem("user")) 
    }
    return this.user;
  }

  userSignedIn(): boolean {
    return JSON.parse(localStorage.getItem("user")) != null ;
  }

  signOut() {
    this.afAuth.signOut();
    localStorage.removeItem("user");
    this.user = null;
  }

  getSuperUsers(){
    return this.firestore.collection("users").doc("superusers").collection("userinfo").valueChanges();
  }



  // News related methods ###############################################################################################################################
  getNews(){
    return this.firestore.collection("news").doc("world").collection("news").valueChanges();
  }

  getNewsCountry(location){
    let country = location;
    return this.firestore.collection("news").doc(country).collection("news").valueChanges();
  }

  submitNews(text){
    let uid = this.user.uid;
    let displayName = this.user.displayName;
    let date = new Date();
    let content = text;
    let news = {
      uid, displayName, date, content
    }
    this.firestore.collection("news").doc("world").collection("news").add(news);
  }

  submitNewsCountry(text, location){
    let uid = this.user.uid;
    let displayName = this.user.displayName;
    let date = new Date();
    let content = text;
    let country = location.toString();
    let news = {
      uid, displayName, date, content, country
    }
    this.firestore.collection("news").doc(country).collection("news").add(news);
  }

  // For Challenge 1 ########################################################################################################################
  submitDocument(document) {
    this.firestore.collection("data").doc(document.country).collection("data").doc(document.country).set(document);
  }
  
  checkUpToDate(country) {
    let country_doc: Document | any;
    country_doc = this.firestore.collection("data").doc(country).collection("data").doc(country).get();
    let current_date = country_doc.date;
    return current_date;
  }

  getDataCountry(location){
    let country = location;
    return this.firestore.collection("data").doc(country).collection("data").doc(country).get();
  }

}

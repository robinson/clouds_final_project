import { Component, } from '@angular/core';
import { ChartDataSets, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css'],
  providers: [DatePipe]
})

export class LineChartComponent {

  monthlyData = new Object;
  cases!: Array<number> | any;
  recovered!: Array<number> | any;
  deaths!: Array<number> | any;
  dates!: Array<string> | any;

  lineChartData: ChartDataSets[] = [];

  lineChartLabels: Label[] = [];
  
  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      //Deaths
      borderColor: 'red',
      backgroundColor: 'rgb(235, 144, 144)',
    },
    {
      //Recovered
      borderColor: 'blue',
      backgroundColor: 'rgb(166, 193, 245)',
    },
    {
      //Active
      borderColor: 'orange',
      backgroundColor: 'rgb(255, 222, 179)',
    }
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';
  
  constructor(private service: DataService, private datePipe: DatePipe) { }

  getAllData() {
    this.service.getData_30_days().subscribe(
      data => {
        this.monthlyData = data;
        this.cases = Object.values(this.monthlyData["cases"]);
        this.recovered = Object.values(this.monthlyData["recovered"]);
        this.deaths = Object.values(this.monthlyData["deaths"]);
        this.dates = Object.keys(this.monthlyData["cases"]);
        for (let date of this.dates ) {
          let index = this.dates.indexOf(date);
          this.dates[index] = this.datePipe.transform(date, 'dd/MM/yy');
        }
      }
    )
  }

  ngOnInit(): void {
    this.getAllData();
  }
}
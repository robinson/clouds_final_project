import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GoogleService } from '../google.service';
import { User, WorldsNews } from '../models';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [DatePipe]
})
export class NewsComponent implements OnInit {

  newsDatabase: Array<WorldsNews> | any;
  world_news: WorldsNews | any;
  uid = new String;
  date = new Date();
  content = new String;
  user: User | any;
  superusers: Array<Object> | any;
  userStatus = new String;

  newsForm = this.formBuilder.group({
    news: '',
  })

  constructor(
    private formBuilder: FormBuilder,
    public googleService: GoogleService,
    private datePipe: DatePipe
  ) { }
  
  submit() {
    this.googleService.submitNews(this.content);
    this.newsForm.reset();
  }

  ngOnInit() {
    this.newsDatabase = [];
    this.googleService.getNews()
    .subscribe(response => {
      this.newsDatabase = response;
    })  
    this.user = this.googleService.getUser();
    this.googleService.getSuperUsers()
      .subscribe(response => {
      this.superusers = response;
      for (let object of this.superusers) {
        if (object.email == this.user.email) {
          this.userStatus = "super";
        }
        else {
          this.userStatus = "guest";
        }
      }
    })
  }
}
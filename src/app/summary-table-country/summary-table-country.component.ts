import { Component, OnInit } from '@angular/core';
import { SummaryData, CountryData } from '../models';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { GoogleService } from '../google.service';

@Component({
  selector: 'app-summary-table-country',
  templateUrl: './summary-table-country.component.html',
  styleUrls: ['./summary-table-country.component.css'],
  providers: [DatePipe]
})
export class SummaryTableCountryComponent implements OnInit {
  summaryData: SummaryData | any;
  country = new String;
  selected_country: SummaryData | any;
  totalConfirmed: number | any;
  newConfirmed: number | any;
  totalRecovered: number | any;
  newRecovered: number | any;
  totalDeaths: number | any;
  newDeaths: number | any;
  countryName = new String;
  document: Document | any;
  test = '';
  current_date = new Date();
  API_date: string | any;

  constructor(private dataService: DataService, private datePipe: DatePipe, private route: ActivatedRoute, private googleService: GoogleService) { }
  
  ngOnInit(): void {
    this.country = this.route.snapshot.params['country'];
    this.getAllData();
    // this.storeAllData(); //This function was only used ONCE to populate the Firestore database, so that we can compare up-to-date status later!
  }

  checkDate() {
    let database_content_date = this.datePipe.transform(this.googleService.checkUpToDate(this.country), "dd/MM/yy");
    let program_date = this.datePipe.transform(new Date(), "dd/MM/yy");
    if (database_content_date == program_date) {
      // We are up to date!
      return true;
    }
    else {
      // We are NOT up to date!
      return false;
    }
  }

  getAllData() {
    // If we are NOT up to date, then we fetch the data from the API and update our database
    if (!this.checkDate()) {
      this.dataService.getData().subscribe(
        response => {
          this.summaryData = response;
          for (let object of this.summaryData?.Countries) {
            if (object.Slug == this.country) {
              this.selected_country = object;
              this.totalConfirmed = this.selected_country.TotalConfirmed;
              this.newConfirmed = this.selected_country.NewConfirmed;
              this.totalRecovered = this.selected_country.TotalRecovered;
              this.newRecovered = this.selected_country.NewRecovered;
              this.totalDeaths = this.selected_country.TotalDeaths;
              this.newDeaths = this.selected_country.NewDeaths;
              this.countryName = this.selected_country.Country;
              this.API_date = this.datePipe.transform(this.selected_country.Date, "dd/MM/yy");
            }
          }
          this.createDocument();
        }
      )
    }
    // If we ARE up to date, then just get the data from our Firestore database
    else {
      let country_data: Document | any; 
      country_data = this.googleService.getDataCountry(this.country);
      this.selected_country = country_data.Slug;
      this.totalConfirmed = country_data.TotalConfirmed;
      this.newConfirmed = country_data.NewConfirmed;
      this.totalRecovered = country_data.TotalRecovered;
      this.newRecovered = country_data.NewRecovered;
      this.totalDeaths = country_data.TotalDeaths;
      this.newDeaths = country_data.NewDeaths;
      this.countryName = country_data.Country;
      this.API_date = this.datePipe.transform(country_data.Date, "dd/MM/yy");
    }
  }

   createDocument() {
     this.document = {
      totalConfirmed: this.totalConfirmed,
      newConfirmed: this.newConfirmed,
      totalecovered: this.totalRecovered,
      newRecovered: this.newRecovered,
      totalDeaths: this.totalDeaths,
      newDeaths: this.newDeaths,
      country: this.selected_country.Slug,
      date: this.API_date
     }
     this.storeDocument();
   }

   storeDocument() {
    this.googleService.submitDocument(this.document);
   }

  //  This function was only used ONCE to populate the database so that we could compare dates! ################################################################
  //  storeAllData() {
  //   this.dataService.getData().subscribe(
  //     response => {
  //       this.summaryData = response;
  //       for (let object of this.summaryData?.Countries) {
  //         this.selected_country = object;
  //         this.totalConfirmed = this.selected_country.TotalConfirmed;
  //         this.newConfirmed = this.selected_country.NewConfirmed;
  //         this.totalRecovered = this.selected_country.TotalRecovered;
  //         this.newRecovered = this.selected_country.NewRecovered;
  //         this.totalDeaths = this.selected_country.TotalDeaths;
  //         this.newDeaths = this.selected_country.NewDeaths;
  //         this.countryName = this.selected_country.Country;
  //         this.API_date = this.datePipe.transform(this.selected_country.Date, "dd/MM/yy");
  //         this.createDocument();
  //         this.storeDocument();
  //       }
  //     }
  //   )
  // }

}
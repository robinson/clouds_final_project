import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryTableCountryComponent } from './summary-table-country.component';

describe('SummaryTableCountryComponent', () => {
  let component: SummaryTableCountryComponent;
  let fixture: ComponentFixture<SummaryTableCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryTableCountryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryTableCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  
  // API Calls ################################################################################################################

  // API Call for Summary Data
  private url_summary: string = "https://api.covid19api.com/summary";
  
  // API Call for 7-days, worldwide
  private url_7_days: string = "https://corona.lmao.ninja/v2/historical/all?lastdays=8";
  
  // API Call for 7-days, individual country
  private base_url_7_days_country_part1: string = "https://corona.lmao.ninja/v2/historical/";
  private base_url_7_days_country_part2: string = "?lastdays=8";
  
  // API Call for Day One, individual country
  private base_url_day_one_country: string = "https://api.covid19api.com/total/dayone/country/";
  
  // API Call for 30 days, worldwide
  private url_30_days: string = "https://corona.lmao.ninja/v2/historical/all";


  // Methods to get data from API Calls ################################################################################################################

  // For initial Summary tables and charts
  getData(): Observable<any> {
    return this.http.get(this.url_summary)
      .pipe((response) => response);
  }

  // For cases in last 7 days worldwide
  getData_7_days(): Observable<any> {
    return this.http.get(this.url_7_days)
      .pipe((data) => data);
  }

  // Get URL for bar chart for one country
  getData_7_days_country(url): Observable<any> {
    return this.http.get(url.toString())
      .pipe((data) => data);
  }

  // For bar chart for one country 
  getURL_Daily(country): String {
    let url_7_days_country = this.base_url_7_days_country_part1 + country + this.base_url_7_days_country_part2;
    return url_7_days_country;
  }

  // For URL for line chart for individual country 
  getURL_Day1(country): String {
    let url_day_one_country = this.base_url_day_one_country + country;
    return url_day_one_country;
  }

  // For line chart for one country
  getData_day_one_country(url): Observable<any> {
    return this.http.get(url.toString())
      .pipe((data) => data);
  }

  // For Total Corona Virus Cases Worldwide (Monthly)
  getData_30_days(): Observable<any> {
    return this.http.get(this.url_30_days)
      .pipe((data) => data);
  }

}

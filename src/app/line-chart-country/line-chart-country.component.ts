import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { SummaryData, DailyData, DayOneData} from '../models';

@Component({
  selector: 'app-line-chart-country',
  templateUrl: './line-chart-country.component.html',
  styleUrls: ['./line-chart-country.component.css'],
  providers: [DatePipe]
})
export class LineChartCountryComponent implements OnInit {

  dayOneData: DayOneData | any;
  cases: Array<number> | any;
  recovered: Array<number> | any;
  deaths: Array<number> | any;
  dates: Array<string> | any;
  dailyData: DailyData | any;
  country = new String;
  countryName = new String;
  summaryData: SummaryData | any;
  selected_country: SummaryData | any;
  url = new String;

  lineChartData: ChartDataSets[] = [];

  lineChartLabels: Label[] = [];
  
  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      //Deaths
      borderColor: 'red',
      backgroundColor: 'rgb(235, 144, 144)',
    },
    {
      //Recovered
      borderColor: 'blue',
      backgroundColor: 'rgb(166, 193, 245)',
    },
    {
      //Active
      borderColor: 'orange',
      backgroundColor: 'rgb(255, 222, 179)',
    }
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';


  constructor(private service: DataService, private datePipe: DatePipe, private route: ActivatedRoute) { }

  getCountryName() {
    this.service.getData().subscribe(
      response => {
        this.summaryData = response;
        for (let object of this.summaryData?.Countries) {
          if (object.Slug == this.country) {
            this.selected_country = object;
            this.countryName = this.selected_country.Slug;
          }
        }
        this.url = this.service.getURL_Day1(this.countryName);
        this.getAllData();
      }
    )
  }

  getAllData() {
    this.service.getData_day_one_country(this.url).subscribe(
      data => {
        this.dayOneData = data;
        for (let object of this.dayOneData) {
          this.cases.push(object.Confirmed);
          this.recovered.push(object.Recovered);
          this.deaths.push(object.Deaths);
          let objectDate: String | any;
          objectDate = this.datePipe.transform(object.Date, 'dd/MM/yy');
          this.dates.push(objectDate);
        }
      }
    )
  }

  ngOnInit(): void {
    this.country = this.route.snapshot.params['country'];
    this.getCountryName();
    this.cases = [];
    this.recovered = [];
    this.deaths = [];
    this.dates = [];
  }


}
import { Component } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css'],
  providers: [DatePipe]
})

export class BarChartComponent {

  dailyData = new Object;
  cases!: Array<number> | any;
  recovered!: Array<number> | any;
  deaths!: Array<number> | any;
  dates!: Array<string> | any;

  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [ {}, {}, {} ];

  constructor(private service: DataService, private datePipe: DatePipe) {  }

  getAllData() {
    this.service.getData_7_days().subscribe(
      data => {
        this.dailyData = data;
        this.cases = Object.values(this.dailyData["cases"]);
        this.recovered = Object.values(this.dailyData["recovered"]);
        this.deaths = Object.values(this.dailyData["deaths"]);
        this.dates = Object.keys(this.dailyData["cases"]);
        for (let date of this.dates ) {
          let index = this.dates.indexOf(date);
          this.dates[index] = this.datePipe.transform(date, 'dd/MM/yy');
        }
      }
    )
  }

  ngOnInit(): void {
    this.getAllData();
  }

}
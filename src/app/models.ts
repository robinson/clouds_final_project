export class SummaryData {
    Global: GlobalData;
    Countries: CountryData;
}

export class GlobalData {
    NewConfirmed: number;
    NewDeaths: number;
    NewRecovered: number;
    TotalConfirmed: number;
    TotalDeaths: number;
    TotalRecovered: number
}

export class CountryData {
    NewConfirmed: number;
    NewDeaths: number;
    NewRecovered: number;
    TotalConfirmed: number;
    TotalDeaths: number;
    TotalRecovered: number
}

export class DailyData {
    country: string;
    province: Array<string>;
    timeline: Array<Object>;
}

export class DayOneData {
    Country: string;
    Confirmed: number;
    Deaths: number;
    Recovered: number;
    Active: number;
}

export interface User {
    uid: string;
    displayName: string;
    email: string;
}

export class WorldsNews {
    uid: string;
    Content: string;
    Date: Date;
}

export class CountryNews {
    uid: string;
    content: string;
    date: Date;
}

export class Document {
    totalConfirmed: number;
    newConfirmed: number;
    totalecovered: number;
    newRecovered: number;
    totalDeaths: number;
    newDeaths: number;
    country: string;
    date: Date;
}
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBgtRMYDVujFpY31KpUNXoTCpUweNu1bAY",
    authDomain: "clouds-final-project.firebaseapp.com",
    projectId: "clouds-final-project",
    storageBucket: "clouds-final-project.appspot.com",
    messagingSenderId: "829360429557",
    appId: "1:829360429557:web:b632a1ba4000d93b2e28bc",
    measurementId: "G-9BFX06C60F"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
